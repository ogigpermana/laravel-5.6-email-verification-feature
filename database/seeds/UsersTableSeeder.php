<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset table user
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();

        // Generate 3 users
        DB::table('users')->insert([
            [
                'first_name' => 'Ogi',
                'last_name' => 'G Permana',
                'slug' => 'ogigpermana',
                'email' => 'ogi@example.com',
                'password' => bcrypt('very_strong_password'),
                'active' => 1,
                'work' => 'I am Junior Programmer',
                'biography' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis euismod tempor. Suspendisse eget ipsum lobortis, viverra metus non, aliquet urna. Ut id ex malesuada, ornare tellus sed, blandit mauris. Suspendisse viverra accumsan quam sit amet bibendum. Phasellus sit amet iaculis nibh. Sed vitae est velit. Aenean vel molestie diam'
            ],
            [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'slug' => 'john-doe',
                'email' => 'johndoe@example.com',
                'password' => bcrypt('very_strong_password'),
                'active' => 1,
                'work' => 'I am UI/UX Designer',
                'biography' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis euismod tempor. Suspendisse eget ipsum lobortis, viverra metus non, aliquet urna. Ut id ex malesuada, ornare tellus sed, blandit mauris. Suspendisse viverra accumsan quam sit amet bibendum. Phasellus sit amet iaculis nibh. Sed vitae est velit. Aenean vel molestie diam'
            ],
            [
                'first_name' => 'Jane',
                'last_name' => 'Doe',
                'slug' => 'jane-doe',
                'email' => 'janedoe@example.com',
                'password' => bcrypt('very_strong_password'),
                'active' => 1,
                'work' => 'I am Software Tester',
                'biography' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis euismod tempor. Suspendisse eget ipsum lobortis, viverra metus non, aliquet urna. Ut id ex malesuada, ornare tellus sed, blandit mauris. Suspendisse viverra accumsan quam sit amet bibendum. Phasellus sit amet iaculis nibh. Sed vitae est velit. Aenean vel molestie diam'
            ] 
        ]);
    }
}
