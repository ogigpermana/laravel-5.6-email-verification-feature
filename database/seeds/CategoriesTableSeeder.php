<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate catefories table
        DB::table('categories')->truncate();
        DB::table('categories')->insert([
            [
                'title' => "Uncategorize",
                'slug' => 'uncategorize'
            ],
            [
                'title' => "PHP",
                'slug' => 'php'
            ],
            [
                'title' => "JAVASCRIPT",
                'slug' => 'javascript'
            ],
            [
                'title' => "PYTHON",
                'slug' => 'python'
            ],
            [
                'title' => "Freebes",
                'slug' => 'freebes'
            ],         
        ]);

        // Update the posts data

        // for($post_id = 1; $post_id <= 50; $post_id++)
        // {
        //     $category_id = rand(1, 5);

        //     DB::table('posts')
        //         ->where('id', $post_id)
        //         ->update(['category_id' => $category_id]);
        // }
    }
}
