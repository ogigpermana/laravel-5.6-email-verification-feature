
@extends('layouts.loginregister')
@section('title', 'Login')
@section('content')
    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">{{ __('Login') }}</div>
        <div class="card-body">
            @include('layouts.components.alert')
          <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="form-group">
              <div class="form-label-group">
                  <input id="inputEmail" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email address" value="{{ old('email') }}" autofocus>

                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                <label for="inputEmail">Email address</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                  <input id="inputPassword" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password">

                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                <label for="inputPassword">Password</label>
              </div>
            </div>
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  Remember Password
                </label>
              </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">
                {{ __('Login') }}
            </button>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
            <a class="d-block small" href="{{ route('auth.activate.resend') }}">{{ __('Resend Activation Email!') }}</a>
            <a class="d-block small mt-3" href="{{ route('register') }}">{{ __('Register an Account') }}</a>
          </div>
        </div>
      </div>
    </div>
@endsection
