
<div class="mt-3">
    <div class="card">
        <h5 class="card-header">Popular Articles</h5>
        <div class="card-body">
            @foreach ($popularPosts as $item)
            <div class="media">
                {{-- <span class="media-left mr-2">
                    @if($item->image)
                    <img class="img-fluid" src="{{$item->image_url}}" alt="{{ $item->title }}" width="100">  
                    @endif
                </span> --}}
                <span>
                    <div class="media-body">
                        <li style="list-style:none"><a href="{{ route('post.show', $item->slug) }}">{{ $item->title }}</a></li>
                    </div>
                </span>
            </div>
            <hr style="border-style:dashed"> 
            @endforeach
        </div>
    </div>
</div>