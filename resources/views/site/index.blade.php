@extends('layouts.main')
@section('title', 'Latest Articles')
@section('content')

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center">
      <div class="container">
        <img class="img-fluid mb-5 d-block mx-auto" src="img/profile.png" alt="">
        <h1 class="text-uppercase mb-0">L56</h1>
        <hr class="star-light">
        <h2 class="font-weight-light mb-0">Web Developer - Graphic Artist - User Experience Designer</h2>
      </div>
    </header>

    <!-- article Grid Section -->
    <section class="article" id="articles">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Latest Articles</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          @foreach ($posts as $post)
            <div class="col-md-6 col-lg-4">
              <a class="article-item d-block mx-auto" href="#article-modal-{{$post->id}}">
                <div class="article-item-caption d-flex position-absolute h-100 w-100">
                  <div class="article-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fas fa-search-plus fa-3x"></i>
                  </div>
                </div>
                @if ($post->image)
                  <img class="img-fluid" src="{{$post->image_url}}" alt="">
                  <p class="text-center"><span class="badge badge-danger badge-circle"><i class="fa fa-folder"></i> {{ $post->category->title }}</span></p>
                @else
                  <img class="img-fluid" src="/img/posts/default.png" alt=""> 
                  <p class="text-center"><span class="badge badge-danger badge-circle"><i class="fa fa-folder"></i> {{ $post->category->title }}</span></p>   
                @endif
              </a>
            </div>
          @endforeach
        </div>
        <div>{{ $posts->links() }}</div>
      </div>
    </section>

    <!-- About Section -->
    <section class="bg-primary text-white mb-0" id="about">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">About</h2>
        <hr class="star-light mb-5">
        <div class="row">
          <div class="col-lg-4 ml-auto">
            <p class="lead">Laravel 5.6 or what ever</p>
          </div>
          <div class="col-lg-4 mr-auto">
            <p class="lead">Laravel 5.6 or what ever</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Contact Section -->
   @include('site.contact.index')

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- article Modals -->

    <!-- article Modal 1 -->
    @foreach ($posts as $post)
    <div class="article-modal mfp-hide" id="article-modal-{{$post->id}}">
      <div class="article-modal-dialog bg-white">
        <a class="close-button d-none d-md-block article-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0">{{$post->title}}</h2>
              <hr class="star-dark mb-5">
              @if ($post->image)
                <img class="img-fluid" src="{{$post->image_url}}" alt="">
                @else
                <img class="img-fluid" src="/img/posts/default.png" alt="">    
                @endif
              <div class="mb-3 mt-3" style="justify-content:center"><strong>Posted by</strong> <span class="badge badge-dark">{{ $post->user->first_name }}</span> | <span><i class="far fa-clock"></i> {{ $post->date }}</span> | <span><i class="far fa-folder"></i> {{ $post->category->title }}</span></div>
              <p class="mb-5">{{ $post->excerpt }}</p>
              <a class="btn btn-primary btn-lg rounded-pill" href="{{route('post.show', $post->slug)}}">
                <i class="fa fa-close"></i>
                Read more</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach

@endsection
    
