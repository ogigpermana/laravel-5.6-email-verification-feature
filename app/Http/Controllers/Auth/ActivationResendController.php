<?php

namespace App\Http\Controllers\Auth;

use App\Models\Actor\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\Auth\UserActivationEmail;

class ActivationResendController extends Controller
{
    public function showResendForm()
    {
        return view('auth.activate.resend');
    }

    public function resend(Request $request)
    {
        $this->validateResendRequest($request);

        $user = User::where('email', $request->email)->first();

        if($user->active == true)
        {
            return redirect()->route('auth.activate.resend')->with('error', 'This email has been activated. You do not need to resend activation link.');
        }
        else
        {
            event(new UserActivationEmail($user));
            return redirect()->route('login')->with('success', 'A fresh verification link has been sent to your email address.');
        }
    }

    public function validateResendRequest(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email'
        ], [
            'email.exists' => 'These credentials do not match our records.'
        ]);
    }
}
