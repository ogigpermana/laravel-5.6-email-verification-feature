@extends('layouts.mainadmin')

@section('content')
        <div class="col-md-12">
            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-primary o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                        <i class="fas fa-fw fa-users"></i>
                        </div>
                        <div class="mr-5">{{ $users->count() }} New {{ str_plural('user', $users->count()) }} !</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="#">
                        <span class="float-left">View Details</span>
                        <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                        </span>
                    </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-warning o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                        <i class="fas fa-fw fa-list"></i>
                        </div>
                        <div class="mr-5">{{ $posts->count() }} New {{ str_plural('Post', $posts->count()) }}!</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="#">
                        <span class="float-left">View Details</span>
                        <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                        </span>
                    </a>
                    </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
                      <div class="card text-white bg-success o-hidden h-100">
                        <div class="card-body">
                          <div class="card-body-icon">
                            <i class="fas fa-fw fa-shopping-cart"></i>
                          </div>
                          <div class="mr-5">123 New Orders!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                          <span class="float-left">View Details</span>
                          <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
                      <div class="card text-white bg-danger o-hidden h-100">
                        <div class="card-body">
                          <div class="card-body-icon">
                            <i class="fas fa-fw fa-life-ring"></i>
                          </div>
                          <div class="mr-5">13 New Tickets!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                          <span class="float-left">View Details</span>
                          <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                          </span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- User Chart Page-->
          <div class="card mb-3">
                <div class="card-header">
                  <i class="fas fa-chart-area"></i>
                  User Chart Page</div>
                <div class="card-body">
                    {!! $chart->html() !!}
                </div>
                @include('layouts.components.userchart')
          </div>
          <!-- Log Acitvity -->
          @include('log.log_activity')
        </div>
@endsection
