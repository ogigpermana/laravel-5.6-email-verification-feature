<?php

namespace App\Http\Controllers;

use DB;

use Charts;
use App\Models\Site\Post;
use App\Models\Actor\User;
use Illuminate\Http\Request;
use App\Models\Site\Log\LogActivity;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // This format for mysql and produce an error on pgsql
        // $users = User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))->get();
        // This format for pgsql
        $users = User::where(DB::raw("(to_char(created_at,'yyyy-mm-dd hh24:mi:ss'))"),date('Y'))->get();
        $chart = Charts::database($users, 'bar', 'highcharts') 

                            ->title("Monthly new registered user") 

                            ->elementLabel("Total Users") 

                            ->dimensions(1000, 500) 

                            ->responsive(true) 

                            ->groupByMonth(date('Y'), true);
        $posts = Post::published()->get();

        return view('home', compact('chart', 'users', 'posts'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function myTestAddToLog()
    {
        \LogActivity::addToLog('My Testing Add To Log.');
        // dd('log insert successfully.');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function logActivity(LogActivity $logs)
    {
        return view('log.log_activity');
    }

}
