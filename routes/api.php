<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// List articles
Route::get('articles', 'Site\ArticlesController@index');

// List single article
Route::get('article/{id}', 'Site\ArticlesController@show');

// Create new article
Route::post('article', 'Site\ArticlesController@store');

// Update article
Route::put('article', 'Site\ArticlesController@store');

// Delete article
Route::delete('article/{id}', 'Site\ArticlesController@destroy');

