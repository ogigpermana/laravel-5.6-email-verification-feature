<?php

namespace App\Http\Controllers\Site;

use App\Models\Site\Tag;
use App\Models\Site\Post;
use App\Models\Actor\User;
use Illuminate\Http\Request;
use App\Models\Site\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    protected $limit = 6;

    public function index()
    {
        $categories = Category::with(['posts' => function($query){
            $query->published();
        }])->orderBy('title', 'asc')->get();

        $posts = Post::with('user')
                    ->latestFirst()
                    ->published()
                    ->simplePaginate($this->limit);

        return view('site.index', compact('posts', 'categories'));
    }

    public function category(Category $category)
    {
        $categories = Category::with(['posts' => function($query){
            $query->published();
        }])->orderBy('title', 'asc')->get();

        $posts = $category->posts()
                    ->with('user')
                    ->latestFirst()
                    ->published()
                    ->paginate($this->limit);

        return view('site.index', compact('posts', 'categories'));
    }

    public function tag(Tag $tag)
    {
        $posts = $tag->posts()
                    ->with('user')
                    ->latestFirst()
                    ->published()
                    ->paginate($this->limit);

        return view('site.index', compact('posts'));
    }

    public function user(User $user)
    {
        $userName = $user->first_name;

        $categories = Category::with(['posts' => function($query){
            $query->published();
        }])->orderBy('title', 'asc')->get();

        $posts = $user->posts()
                    ->with('category')
                    ->latestFirst()
                    ->published()
                    ->paginate($this->limit);

        return view('site.index', compact('posts', 'userName', 'categories'));
    }

    public function show(Post $post)
    {

        $ids = Session::get('ids') ? Session::get('ids') : [];
        if (!in_array($post->id, $ids)) 
        {
            $post->increment('view_count');
            $ids[] = $post->id;
            Session::put('ids', $ids);
        }

        $relatedPosts = Post::whereHas('tags', function($q) use ($post){
            return $q->whereIn('name', $post->tags->pluck('name'));
        })->published()->take(5)->where('id', '!=', $post->id)->get();

        return view('site.show', compact('post', 'relatedPosts'));
    }
}
