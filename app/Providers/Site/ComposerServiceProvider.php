<?php

namespace App\Providers\Site;

use App\Models\Site\Tag;
use App\Models\Site\Post;
use App\Models\Site\Category;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.main', function ($view) {
            $categories = Category::with(['posts' => function($query){
                $query->published();
            }])->orderBy('title', 'asc')->get();

            return $view->with('categories', $categories);
        });

        view()->composer('site.sidebar', function($view){
            $tags = Tag::has('posts')->get();
            return $view->with('tags', $tags);
        });

        view()->composer('layouts.sidebar', function($view){
            $popularPosts = Post::published()->popular()->take(5)->get();
            return $view->with('popularPosts', $popularPosts);
        });

        view()->composer('home', function($view){
            $logs = \LogActivity::logActivityLists();
            return $view->with('logs', $logs);
        });

        // view()->composer('site.show', function($view){
        //     $relatedPosts = Post::whereHas('tags', function($q) use ($post){
        //         return $q->whereIn('name', $post->tags->pluck('name'));
        //     })->published()->take(5)->where('id', '!=', $post->id)->get();
        //     return $view->with('relatedPosts', $relatedPosts);
        // });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
