<?php

use App\Models\Site\Tag;
use App\Models\Site\Post;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('tags')->truncate();

        $php = new Tag();
        $php->name = "PHP";
        $php->slug = "php";
        $php->save();

        $laravel = new Tag();
        $laravel->name = "Laravel";
        $laravel->slug = "laravel";
        $laravel->save();

        $python = new Tag();
        $python->name = "Python";
        $python->slug = "python";
        $python->save();

        $javascript = new Tag();
        $javascript->name = "Javascript";
        $javascript->slug = "javascript";
        $javascript->save();


        $tags = [
            $php->id,
            $laravel->id,
            $python->id,
            $javascript->id
        ];

        foreach (Post::all() as $post) {
            shuffle($tags); // to random array so it could be variated

            for ($i=0; $i < rand(0, count($tags)-1) ; $i++) { 
                $post->tags()->detach($tags[$i]);
                $post->tags()->attach($tags[$i]);
            }
        }
    }
}
