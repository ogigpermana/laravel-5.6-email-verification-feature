@extends('layouts.mainadmin')
@section('title', 'Display Articles')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Post List
                        <span class="float-right"><a onclick="#" class="btn btn-primary " style="color:white"><i class="fas fw fa-plus"></i> Add new post</a></span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="post-table" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="30">No</th>
                                        <th>Title</th>
                                        <th>Created Date</th>
                                        <th width="75">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.components.scriptpost')
@endsection