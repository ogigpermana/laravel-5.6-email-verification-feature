<?php

namespace App\Models\Actor;

use App\Models\Site\Post;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'slug', 'email', 'password', 'active', 'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function scopeByActivationColumns(Builder $builder, $email, $token)
    {
        return $builder->where('email', $email)->where('activation_token', $token);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function gravatar()
    {
        $email = $this->email;
        $default = "https://api.adorable.io/avatars/285/putraranji.png";
        $size = 40;

        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
    }

    public function getBioHtmlAttribute($value)
    {
        return $this->biography ? Markdown::convertToHtml(e($this->biography)) : NULL;
    }
}
