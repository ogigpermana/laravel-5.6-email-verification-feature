<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset the posts table
        DB::table('posts')->truncate();

        // Generate 10 dummy posts data
        $posts = [];
        $faker = Faker\Factory::create();
        $date = Carbon::create(2018, 10, 7, 9);
        
        for ($i=1; $i <= 50 ; $i++) { 
            $image = "post_" . rand(1, 6) . ".png";
            // $date = date("Y-m-d H:i:s", strtotime("2018-10-08 11:00:00 + {$i} days"));
            $date->addDays(1);
            $publishedDate = clone($date);
            $createdDate = clone($date);
            
            $posts[] = [
                'user_id' => rand(1, 3),
                'category_id' => rand(1, 5),
                'title' => $faker->sentence(rand(4, 6)),
                'slug' => $faker->slug(),
                'excerpt' => $faker->text(rand(250, 300)),
                'body' => $faker->paragraphs(rand(10, 15), true),
                'image' => rand(0, 1) == 1 ? $image : NULL,
                'created_at' => $createdDate,
                'updated_at' => $createdDate,
                'published_at' =>$i < 5 ? $publishedDate : (rand(0, 1) == 0 ? NULL: $publishedDate->addDays(4)),
                'view_count' => rand(1, 10) * 10
            ];
        }

        DB::table('posts')->insert($posts);
    }
}
