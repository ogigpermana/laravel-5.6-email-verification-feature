@extends('layouts.loginregister')
@section('title', 'Resend Intruction')
@section('content')
<div class="container">
        <div class="card card-login mx-auto mt-5">
          <div class="card-header">{{ __('Resend Activation Email') }}<</div>
          <div class="card-body">
                @include('layouts.components.alert')
            <div class="text-center mb-4">
              <h4>Resend Activation Link</h4>
              <p>Enter your email address and we will resend you email activation link.</p>
            </div>
            <form method="POST" action="{{ route('auth.activate.resend') }}" aria-label="{{ __('Resend Activation Email') }}">
                @csrf
              <div class="form-group">
                <div class="form-label-group">
                  <input type="email" id="inputEmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter email address" required="required" autofocus="autofocus">
                  @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                  <label for="inputEmail">{{ __('E-Mail Address') }}</label>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Resend') }}
              </button>
            </form>
            <div class="text-center">
              <a class="d-block small mt-3" href="{{ route('register') }}">Register an Account</a>
              <a class="d-block small" href="{{ route('login') }}">Login Page</a>
            </div>
          </div>
        </div>
      </div>
@endsection