
<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>L56 Articles</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="/dist/js/require.min.js"></script>
    <script>
      requirejs.config({
          baseUrl: '.'
      });
    </script>
    <!-- Dashboard Core -->
    <link href="/dist/css/dashboard.css" rel="stylesheet" />
    <script src="/dist/js/dashboard.js"></script>
    <!-- c3.js Charts Plugin -->
    <link href="/dist/plugins/charts-c3/plugin.css" rel="stylesheet" />
    <script src="/dist/plugins/charts-c3/plugin.js"></script>
    <!-- Google Maps Plugin -->
    <link href="/dist/plugins/maps-google/plugin.css" rel="stylesheet" />
    <script src="/dist/plugins/maps-google/plugin.js"></script>
    <!-- Input Mask Plugin -->
    <script src="/dist/plugins/input-mask/plugin.js"></script>
  </head>
  <body class="">
    <div id="app">
        <div class="page">
            <div class="page-main">
                <navbar></navbar>
                <sidebar></sidebar>
                <div class="my-3 my-md-5">
                <div class="container">
                <articles></articles>
                </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
                    Copyright © 2018 {{config('app.name')}}
                    </div>
                </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="/js/app.js"></script>
  </body>
</html>