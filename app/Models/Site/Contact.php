<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $table = 'contacts';

    public $fillable = ['name','email','phone','message'];
}
