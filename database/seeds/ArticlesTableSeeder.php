<?php

use App\Models\Site\Article;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the article table
        // DB::table('articles')->truncate();
        
        factory(Article::class, 30)->create();
    }
}
