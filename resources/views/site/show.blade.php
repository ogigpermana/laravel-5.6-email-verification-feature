@extends('layouts.main')
@section('title')
{{$post->title}}
@endsection
@section('content')
<div class="container" style="margin-top:100px">
    <div class="row">
        <div class="my-3 my-md-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 order-lg-1 mb-4">
                        <div class="card">
                            <!-- user profile image -->
                            <img class="card-img-top" src="{{ $post->user->gravatar() }}" alt="{{ $post->user->first_name }} {{ $post->user->last_name }}">
                            <div class="card-body text-center">
                                <!-- user name -->
                                <h4 class="card-title">{{ $post->user->first_name }} {{ $post->user->last_name }}</h4>
                                <!-- job title / comany name -->
                                <p class="card-text text-muted">{{ $post->user->work }}</p>
                                <p class="card-text text-muted">Bio: {!! $post->user->bio_html !!}</p>
                                <?php $postCount = $post->user->posts()->published()->count(); ?>
                                <p class="card-text text-muted">{{ $postCount }}  {{ str_plural('post', $postCount) }}</p>
                                <!-- social profile links -->
                                <a href="#" class="social twitter"><i class="fab fa-fw fa-twitter"></i></a>
                                <a href="#" class="social facebook"><i class="fab fa-fw fa-facebook"></i></a>
                                <a href="#" class="social google-plus"><i class="fab fa-fw fa-google-plus"></i></a>
                                <a href="#" class="social linkedin"><i class="fab fa-fw fa-linkedin"></i></a>
                                <!-- CTA button -->
                                <p class="mt-4"><a href="{{ url('/') }}#contact" class="btn btn-primary">Contact me!</a></p>
                            </div>
                        </div>
                        @include('layouts.sidebar')
                    </div>
                    <div class="col-lg-9">
                        <div class="blog-grids">
                            <div class="grid">
                                <div class="entry-media">
                                    @if ($post->image)
                                    <img src="{{ $post->image_url }}" alt="{{ $post->title }}">                                        
                                    @endif
                                </div>
                                <div class="entry-body">
                                    <h2>{{ $post->title }}</h2>
                                    <span class="cat">BY <a href="{{ route('user', $post->user->slug)}}">{{ $post->user->first_name }}</a></span>
                                    <span class="cat">under <a href="{{ route('category', $post->category->slug) }}">{{ $post->category->title }}</a></span>
                                    <p>{!! $post->body_html !!}</p>
                                    <div class="row">
                                        <div class="col-sm">
                                        Posted {{ $post->date }}
                                        </div>
                                        <div class="col-sm">
                                            Tagged :
                                           @foreach ($post->tags as $item)
                                                <a href="{{ route('tag', $item->slug) }}" class="badge badge-secondary">{{ $item->name }}</a>
                                           @endforeach
                                        </div>
                                    </div>
                                    <hr style="border-style:dashed;">
                                    <div class="container">
                                        <div class="row">
                                            <h3 class="badge badge-primary">Related Articles</h3>
                                            @if ($relatedPosts->count() > 0)
                                            <div class="col-md-12">
                                                @foreach ($relatedPosts as $post)
                                                <div>
                                                    <h3><a href="{{ route('post.show', $post->slug) }}">{{ $post->title }}</a></h3>
                                                    <p>{{ str_limit($post->excerpt, 150) }} - <span class="badge badge-secondary">{{ $post->date }}</span></p>
                                                </div>
                                                @endforeach
                                            <div>
                                            @else
                                             <div class="col-md-12">
                                                 <p>There's no related article could be displayed at this moment</p>
                                             </div>   
                                            @endif
                                        </div>
                                        </div>
                                    </div>  
                                  </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
@endsection