<?php

Route::get('/', 'Site\PostController@index');

Route::get('/read/{post}', 'Site\PostController@show')->name('post.show');

Route::get('/category/{category}', 'Site\PostController@category')->name('category');

Route::get('/user/{user}', 'Site\PostController@user')->name('user');

Route::get('/tag/{tag}', 'Site\PostController@tag')->name('tag');

Route::get('testinfo', function(){
    echo phpinfo();
});

Route::post('contact', 'Site\ContactController@contactPost')->name('contact.store');

Auth::routes();

Route::get('/auth/activate', 'Auth\ActivationController@activate')->name('auth.activate');

Route::get('/auth/activate/resend','Auth\ActivationResendController@showResendForm');
Route::post('/auth/activate/resend', 'Auth\ActivationResendController@resend')->name('auth.activate.resend');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('add-to-log', 'HomeController@myTestAddToLog');
Route::get('logActivity', 'HomeController@logActivity');

// Admin Area
Route::resource('/dashboard/post', 'Admin\Site\PostController');

Route::get('/api/postcollection', 'Admin\Site\PostController@postCollection')->name('post.collection');