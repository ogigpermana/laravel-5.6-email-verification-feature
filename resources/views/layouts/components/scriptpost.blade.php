@section('postcollection')
<script type="text/javascript">
    $('#post-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('post.collection') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    })
</script> 
@stop