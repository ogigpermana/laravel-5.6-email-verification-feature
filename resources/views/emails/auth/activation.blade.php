@component('mail::message')
# Activation email

Thanks for signing up to our website, please activate your account.

@component('mail::button', ['url' => route('auth.activate', [
    'token' => $user->activation_token,
    'email' => $user->email
])])
Activate
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
